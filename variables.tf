variable "prefix" {
  description = "The prefix which should be used for all resources in this example"
  default     = "sf"
}

variable "location" {
  description = "The Azure Region in which all resources in this example should be created. Use the `Name` value from `az account list-locations -otable` for your chosen region"
  default     = "westeurope"
}

variable "tags" {
  type = map(string)

  default = {
    source      = "terraform"
    environment = "prod"
    managed_by  = "terraform"
  }
}

variable "storage_account_tier" {
  description = "Tier for storage account"
  default     = "standard"
}

variable "storage_replication_type" {
  description = "Replication type for storage account"
  default     = "GRS"
}

variable "keyvault_sku" {
  description = "sku for keyvault. Available options standard amd premium"
  default     = "standard"
}

variable "public_ip_allocation" {
  description = "Defines the allocation method for this IP address. Possible values are Static or Dynamic"
  default     = "Dynamic"
}

variable "public_ip_domain_name_label" {
  description = " Label for the Domain Name. Will be used to make up the FQDN. If a domain name label is specified, an A DNS record is created for the public IP in the Microsoft Azure DNS system."
  default     = "servicefabric"
}

variable "service_fabric_cluster_reliability_level" {
  description = "pecifies the Reliability Level of the Cluster. Possible values include None, Bronze, Silver, Gold and Platinum."
  default     = "Bronze"
}

variable "service_fabric_upgrade_mode" {
  description = "Specifies the Upgrade Mode of the cluster. Possible values are Automatic or Manual."
  default     = "Automatic"
}

variable "service_fabric_vm_image" {
  description = "Specifies the Image expected for the Service Fabric Cluster, such as Windows. Changing this forces a new resource to be created."
  default     = "Windows"
}

variable "service_fabric_instance_count" {
  description = "The number of nodes for this Node Type."
  default     = 3

}

variable "vmss_sku" {
  description = "The Virtual Machine SKU for the Scale Set, such as Standard_F2."
  default     = "Standard_D1_v2"
}

variable "source_image_publisher" {
  default = "MicrosoftWindowsServer"
}

variable "source_image_offer" {
  default = "WindowsServer"
}

variable "source_image_sku" {
  default = "2016-Datacenter-with-Containers"
}

variable "os_disk_storage_account_type" {
  description = "The Type of Storage Account which should back this the Internal OS Disk. Possible values include Standard_LRS, StandardSSD_LRS and Premium_LRS"
  default     = "Standard_LRS"
}

variable "os_disk_caching" {
  description = "The Type of Caching which should be used for the Internal OS Disk. Possible values are None, ReadOnly and ReadWrite."
  default     = "ReadWrite"
}

variable "vmss_network_interface_name" {
  description = "The Name which should be used for this Network Interface. Changing this forces a new resource to be created."
  default     = "service_fabric_vmss_network_interface"
}

variable "vmss_extension_publisher" {
  description = " Specifies the Publisher of the Extension."
  default     = "Microsoft.Azure.ServiceFabric"
}

variable "vmss_extension_type" {
  default = "ServiceFabricNode"
}

variable "vmss_extension_type_handler" {
  default = "1.1"
}