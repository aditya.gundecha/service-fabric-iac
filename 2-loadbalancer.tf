resource "azurerm_public_ip" "service_fabric_public_ip" {
  name                = "${var.prefix}PIPForLB"
  location            = azurerm_resource_group.service_fabric_resource_group.location
  resource_group_name = azurerm_resource_group.service_fabric_resource_group.name
  allocation_method   = var.public_ip_allocation
  domain_name_label   = var.public_ip_domain_name_label
}

resource "azurerm_lb" "service_fabric_load_balancer" {
  name                = "${var.prefix}LoadBalancer"
  location            = azurerm_resource_group.service_fabric_resource_group.location
  resource_group_name = azurerm_resource_group.service_fabric_resource_group.name

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = azurerm_public_ip.service_fabric_public_ip.id
  }
}

resource "azurerm_lb_backend_address_pool" "service_fabric_load_balancer_backend_address_pool" {
  resource_group_name = azurerm_resource_group.service_fabric_resource_group.name
  loadbalancer_id     = azurerm_lb.service_fabric_load_balancer.id
  name                = "${var.prefix}BEAPool"
}

resource "azurerm_lb_nat_pool" "service_fabric_load_balancer_nat_pool" {
  resource_group_name            = azurerm_resource_group.service_fabric_resource_group.name
  loadbalancer_id                = azurerm_lb.service_fabric_load_balancer.id
  name                           = "${var.prefix}SFApplicationPool"
  protocol                       = "Tcp"
  frontend_port_start            = 3389
  frontend_port_end              = 4500
  backend_port                   = 3389
  frontend_ip_configuration_name = azurerm_lb.service_fabric_load_balancer.frontend_ip_configuration[0].name
}

resource "azurerm_lb_rule" "service_fabric_load_balancer_tcp_rule" {
  resource_group_name            = azurerm_resource_group.service_fabric_resource_group.name
  loadbalancer_id                = azurerm_lb.service_fabric_load_balancer.id
  name                           = "${var.prefix}LBRuleTcp"
  protocol                       = "Tcp"
  frontend_port                  = 19000
  backend_port                   = 19000
  frontend_ip_configuration_name = azurerm_lb.service_fabric_load_balancer.frontend_ip_configuration[0].name
  backend_address_pool_id        = azurerm_lb_backend_address_pool.service_fabric_load_balancer_backend_address_pool.id
}

resource "azurerm_lb_rule" "service_fabric_load_balancer_http_rule" {
  resource_group_name            = azurerm_resource_group.service_fabric_resource_group.name
  loadbalancer_id                = azurerm_lb.service_fabric_load_balancer.id
  name                           = "${var.prefix}LBRuleHttp"
  protocol                       = "Tcp"
  frontend_port                  = 19080
  backend_port                   = 19080
  frontend_ip_configuration_name = azurerm_lb.service_fabric_load_balancer.frontend_ip_configuration[0].name
  backend_address_pool_id        = azurerm_lb_backend_address_pool.service_fabric_load_balancer_backend_address_pool.id
}

resource "azurerm_lb_probe" "service_fabric_load_balancer_probe_tcp" {
  resource_group_name = azurerm_resource_group.service_fabric_resource_group.name
  loadbalancer_id     = azurerm_lb.service_fabric_load_balancer.id
  name                = "${var.prefix}SFTcpGatewayProbe"
  port                = 19000
}

resource "azurerm_lb_probe" "service_fabric_load_balancer_probe_http" {
  resource_group_name = azurerm_resource_group.service_fabric_resource_group.name
  loadbalancer_id     = azurerm_lb.service_fabric_load_balancer.id
  name                = "${var.prefix}SFHttpGatewayProbe"
  port                = 19080
}

