terraform {
  required_providers {
    azurerm = "=2.85.0"
    random  = "=3.1.0"
  }
  required_version = "0.14.8"
}
