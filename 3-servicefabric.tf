
resource "azurerm_service_fabric_cluster" "service_fabric_cluster" {
  name                = "${var.prefix}servicefabric"
  resource_group_name = azurerm_resource_group.service_fabric_resource_group.name
  location            = azurerm_resource_group.service_fabric_resource_group.location
  reliability_level   = var.service_fabric_cluster_reliability_level
  upgrade_mode        = var.service_fabric_upgrade_mode
  vm_image            = var.service_fabric_vm_image
  management_endpoint = "https://${var.prefix}servicefabric.${var.location}.cloudapp.azure.com:19080"

  node_type {
    name                 = "Windows"
    instance_count       = var.service_fabric_instance_count
    is_primary           = true
    client_endpoint_port = 19000
    http_endpoint_port   = 19080
  }


  reverse_proxy_certificate {
    thumbprint      = azurerm_key_vault_certificate.service_fabric_keyvault_certificate.thumbprint
    x509_store_name = "My"
  }

  certificate {
    thumbprint      = azurerm_key_vault_certificate.service_fabric_keyvault_certificate.thumbprint
    x509_store_name = "My"
  }

  client_certificate_thumbprint {
    thumbprint = azurerm_key_vault_certificate.service_fabric_keyvault_certificate.thumbprint
    is_admin   = true
  }
}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "azurerm_key_vault_secret" "example" {
  name         = "admin-password"
  value        = random_password.password.result
  key_vault_id = azurerm_key_vault.service_fabric_keyvault.id
}

resource "azurerm_windows_virtual_machine_scale_set" "service_fabric_vmss" {
  name                 = "${var.prefix}examplesf"
  computer_name_prefix = var.prefix
  resource_group_name  = azurerm_resource_group.service_fabric_resource_group.name
  location             = azurerm_resource_group.service_fabric_resource_group.location
  sku                  = var.vmss_sku
  instances            = azurerm_service_fabric_cluster.service_fabric_cluster.node_type[0].instance_count
  admin_password       = random_password.password.result
  admin_username       = "adminuser"
  overprovision        = false
  upgrade_mode         = "Automatic"
  tags                 = var.tags


  source_image_reference {
    publisher = var.source_image_publisher
    offer     = var.source_image_offer
    sku       = var.source_image_sku
    version   = "latest"
  }

  os_disk {
    storage_account_type = var.os_disk_storage_account_type
    caching              = var.os_disk_caching
  }

  network_interface {
    name    = var.vmss_network_interface_name
    primary = true

    ip_configuration {
      name      = "internal"
      primary   = true
      subnet_id = azurerm_subnet.service_fabric_subnet.id
      load_balancer_backend_address_pool_ids = [
        azurerm_lb_backend_address_pool.service_fabric_load_balancer_backend_address_pool.id
      ]
      load_balancer_inbound_nat_rules_ids = [
        azurerm_lb_nat_pool.service_fabric_load_balancer_nat_pool.id
      ]
    }
  }

  secret {
    certificate {
      store = "My"
      url   = azurerm_key_vault_certificate.service_fabric_keyvault_certificate.secret_id
    }
    key_vault_id = azurerm_key_vault.service_fabric_keyvault.id
  }

  extension {
    name                       = "${var.prefix}ServiceFabricNode"
    publisher                  = var.vmss_extension_publisher
    type                       = var.vmss_extension_type
    type_handler_version       = var.vmss_extension_type_handler
    auto_upgrade_minor_version = false

    settings = jsonencode({
      "clusterEndpoint"    = azurerm_service_fabric_cluster.service_fabric_cluster.cluster_endpoint
      "nodeTypeRef"        = azurerm_service_fabric_cluster.service_fabric_cluster.node_type[0].name
      "durabilityLevel"    = var.service_fabric_cluster_reliability_level
      "nicPrefixOverride"  = azurerm_subnet.service_fabric_subnet.address_prefixes[0]
      "enableParallelJobs" = true
      "certificate" = {
        "commonNames" = [
          "${var.prefix}servicefabric.${var.location}.cloudapp.azure.com",
        ]
        "x509StoreName" = "My"
      }
    })

    protected_settings = jsonencode({
      "StorageAccountKey1" = azurerm_storage_account.service_fabric_storage_account.primary_access_key
      "StorageAccountKey2" = azurerm_storage_account.service_fabric_storage_account.secondary_access_key
    })
  }

}
