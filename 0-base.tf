provider "azurerm" {
  features {}
}

data "azurerm_client_config" "current" {
}

resource "azurerm_resource_group" "service_fabric_resource_group" {
  name     = "${var.prefix}-resources"
  location = var.location
  tags     = var.tags
}

resource "azurerm_virtual_network" "service_fabric_vnet" {
  name                = "${var.prefix}-sf-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.service_fabric_resource_group.location
  resource_group_name = azurerm_resource_group.service_fabric_resource_group.name
  tags                = var.tags
}

resource "azurerm_subnet" "service_fabric_subnet" {
  name                 = "${var.prefix}-subnet"
  resource_group_name  = azurerm_resource_group.service_fabric_resource_group.name
  virtual_network_name = azurerm_virtual_network.service_fabric_vnet.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_storage_account" "service_fabric_storage_account" {
  name                     = "${var.prefix}sfsa"
  resource_group_name      = azurerm_resource_group.service_fabric_resource_group.name
  location                 = azurerm_resource_group.service_fabric_resource_group.location
  account_tier             = var.storage_account_tier
  account_replication_type = var.storage_replication_type
  tags                     = var.tags
}